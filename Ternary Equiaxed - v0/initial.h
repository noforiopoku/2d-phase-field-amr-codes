// Ternary amr, Jan 2015: based on binary amr from Nov 2014 (Sebastian)
// Reference concentration is co_l=(ca_o,cb_o), respectively.
// Reference temperature is To=Tl

#ifndef INITDEF
#define INITDEF

#include <cmath>

//*******OpenMP variables********//
const int numprocs = 8;		//maximum number of threads

const double Pi = (2.*acos(0.));
extern unsigned int seed;

//runtime and output
const int endt = 250000 ;				//Total number of iterations
const int printFreq = 5000 ;			//printing frequency

const double dt = 0.002;				//time step, stability criterion in 2D: dt<dx^2/(4*max(Db,Dc))

const double Beta = .0;					// parameter to include concentration gradients in adaption
const double AdaptThresh = .001;		// adaption criteria threshold
const int AdaptFreq = 100;				//adaption time cycle

//domain and size parameters
const int resolution = 8 ;				//min dx = size / 2^(resolution-1)
const double size = 100 ;				//Size of one domain in units of W
const int nx = 10 ;						//Number of domains along the x direction
const int ny = 10 ;						//Number of domains along the y direction
const int PBoundaries = 0;				//Set to 1 for Periodic boundaries, non-periodic otherwise.

//Material parameters:
const double Tm = 515.08;					//Melting temperature (K) for pure magnesium
const double cb_o = 1.4;					//Average alloy composition for species b in wt%
const double cc_o = 0.95;					//Average alloy composition for species c in wt%
const double mb = 2.9 ;						//Absolute value of liquidus slope of species b in K/wt%
const double mc = 5.5 ;						//Absolute value of liquidus slope of species c in K/wt%
const double kb = 0.3 ;						//Partition coefficient of species b
const double kc = fabs(1.0-(1.0-kb)*mc/mb);	//Partition coefficient of species c
const double Dlb = 2.8E-11;					//Liquid diffusion coefficient of species b in m^2/s (in the liquid)
const double Dlc = 1.1E-10;					//Liquid diffusion coefficient of species c in m^2/s (in the liquid)
const double Gamma = 9.E-8;					//Gibbs-thomson coefficient in units of K*m
	 
const double Tl = Tm - mb*cb_o - mc*cc_o;					//Liquidus temperature 
const double Ts = Tm - mb*cb_o/kb - mc*cc_o/kc;				//Solidus temperature
const double DT_o = mb*cb_o*(1-kb) + mc*cc_o*(1-kc); 		//Freezing range (at c=k*c_o)
const double DTb_o = mb*cb_o*(1-kb); 						//Partial freezing range for species b
const double DTc_o = mc*cc_o*(1-kc); 						//Partial freezing range for species c
const double d_o = Gamma/DT_o;								//Capillary length

const double omega = 0.2;						//Supersaturation
const double T_init = Tl + omega * (Ts-Tl);  	//Initial system temperature in Kelvin
const double Q_t = 0.0; 						//Cooling rate in Kelvin/sec
const double lbTemp = 0.0;						//Lower bound temperature in Kelvin

//Cooling run flag (sets initial conc profile for no-nucleation runs):
const int coolFlag = 0 ;	//0 = no cooling (quench), 1 = with cooling rate, or any temperature varying system

//******** phase field simulation parameters ********//

const double W = 1.E-7;			//interface width in meters
const double Fu = 1.E-8;		//noise strength/amplitude of gaussian white noise in composition field

//dimensionless variables
const double a1 = 0.8839;
const double a2 = 0.6267;
const double at = 0.35355;		//Antitrapping coefficient
const double L = a1*W/d_o;		
const double Lb = a1*W*DTb_o/Gamma;
const double Lc = a1*W*DTc_o/Gamma;
const double tau = a2*W*W*(Lb/Dlb+Lc/Dlc);	//time constant, scales atomic attachment kinetics
const double Db = tau*Dlb/(W*W);			//dimensionless diffusion coefficient for species b
const double Dc = tau*Dlc/(W*W);			//dimensionless diffusion coefficient for species c
const double Fu_dt = Fu/dt;					//coefficient preceeding uncorrelated noise

//*** Anisotropy Components ***//
const double sym = 4.;			//crystal symmetry (4-four fold, 6-six fold etc)	
const double Es = .03;			//anisotropy strength

const double Rg = 8.3144 ; // Gas constant J/mol/K

// Interdiffusion and Self-diffusion in the solid:
const double fct = tau/(W*W);
// coefficient and activation energies for all 3 species:
// Species 1 = b  (SOLUTE)
const double D1self = 0.395E-4*fct ; 	// m^2/sec
const double Q1self = 184.6E3; 			// J/mol
// Species 2 = c  (SOLUTE)
const double D2self = 0.2E-4*fct; 		// m^2/sec
const double Q2self = 197.3E3; 			// J/mol
// Species 3 = a  (SOLVENT)
const double D3self = 1.4E-4*fct; 		// m^2/sec
const double Q3self = 97.6E3; 			// J/mol

/*******Multi Grain Parameters*******/
const int ng = 3 ;						//number of phase fields
const double obs = 152.173 ;			//magnitude of obstacle 
const double maxmo = 2.*Pi*15./180. ;	//maximum misorientation for RS is applicable (15 degrees)
const int oriFlag = 0;					//flag for either random (0) or uniform (non-zero) orientations for grains
										//if uniform is choosen, orientations will fall within equal intervals within the symmetry of crystal
double * const ang = new double[ng];
double * const dumypsi = new double[ng*ng];
double ** const psi = new double*[ng];

//Nucleation parameters
const int nucrun = 1 ;				//flag to determine if this is a nucleation run (0-no, 1-yes)
const int nucstart = 1;				//nucleation starts at this iteration
const int nucfreq =  100 ;			//nulceation frequency (nucfreq<=adapt_freq)

const double seed_sepa = 8. ;		// For a non-nucleation run, minimum distance between seeds (random) positions

const double RADIUS = 3.0; 			//Dimensionless RADIUS
const double ampnuc = RADIUS+5;		//mesh range of nuclei seed

const double rho = 5.77E6;       	//Density of Solvent, species 3 = c (g/m^3)
const double mm = 118.71;         	//Atomic weight of Solvent, species 3 = c
const double v_o = mm/rho;       	//Molar volume (m^3)/mol
const double nav = 6.02214E23;   	//Avogadro's number
const double Na = nav/v_o;			//Number of atoms per volume

const double Nsl = 20.0 ;
const double fpar = pow(Nsl/(nx*ny*size*size*W*W),1.5)/Na ;	//Nucleation seeds fraction;

const double size3d = nx*nx*ny*size*size*size;	//3D system size
const int Ns = (int)(fpar*Na*size3d*W*W*W);		//Number of seeds for total 3D system
const double hetpre = 6.E-5 ;					//Heterogeneous nucleation prefactor // lower to promote nucleation

//There should be no reason why you would need to change anything below this comment//
const int padlevel = 0;						//Level of which the interface is padded with nodes
const double w = size;						//Width of a single domain
const double h = size;						//Height of a single domain

const double XCen = (double)size*nx/2.0;	//X Center of entire domain
const double YCen = (double)size*ny/2.0;	//Y Center of entire domain

const int Numx = 4*nx;						//Number of initial nodes needed along x
const int Numy = 4*ny;						//Number of initial nodes needed along y

//Temperature matrix dimensions
const int noys=2;								//No. position vals
const int nots=2;								//No. time vals

#endif
