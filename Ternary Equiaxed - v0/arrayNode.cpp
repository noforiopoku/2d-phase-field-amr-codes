#include "arrayNode.h"


	arrayNode::arrayNode()
	{
		int ii;
		Cb = 1.;
		Cc = 1.;
		T = 0.0;
		D11=0.0;
		D12=0.0;
		D21=0.0;
		D22=0.0;
		for (ii=0;ii<ng;ii++)
		{
			Phi[ii] = -1.0;
			dPdt[ii] = 0.0;			
			A[ii] = 0.0;
			dA[ii] = 0.0;
			dPhix[ii] = 0.0;
			dPhiy[ii] = 0.0;
		}
	}
	arrayNode::~arrayNode()
	{
		
	}
	void arrayNode::updatePhi(int npt)
    {
		int ii;

		for(ii=0;ii<npt;ii++)
			Phi[ii] = Phi[ii] + dPdt[ii]*dt;		
    }
	void arrayNode::updateC()
    {
		Cb = Cb + dCbdt*dt;
		Cc = Cc + dCcdt*dt;
	}
		void arrayNode::updateD()
	{
		double d1,d2,d3;
		
		d1 = D1self*exp(-1.0*Q1self/(Rg*T)); // species b 
		d2 = D2self*exp(-1.0*Q2self/(Rg*T)); // species c 
		d3 = D3self*exp(-1.0*Q3self/(Rg*T)); // species a (solvent)
        
        D11 = d1-(d1-d3)*Cb;
        D12 = (-d2+d3)*Cb;
        D21 = (-d1+d3)*Cc;
        D22 = d2-(d2-d3)*Cc;
	}
