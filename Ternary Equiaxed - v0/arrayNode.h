#ifndef ARRAYNODEDEF
#define ARRAYNODEDEF

#include <iostream>
#include <vector>
#include <list>
#include "initial.h"

using namespace std;

class arrayNode
{
    public:
        double x,y,dx;
        double Phi[ng],dPdt[ng],A[ng],dA[ng],dPhix[ng],dPhiy[ng],sumphi;
        double Cb,Cc,dCbdt,dCcdt,eUb,eUc,qb,qc,qbc,qcb,Cbx,Ccx,Cby,Ccy;   //concentration flux arrays
        double u1b,u1c,u2b,u2c;		        //arrays for random currents, i.e. noise
        //Create new variable T
        double T;
        double D11,D12,D21,D22;

        int NN[4];
        int N25[5][5];
        arrayNode();
        ~arrayNode();
        void updatePhi(int npt);
        void updateC();
        void updateD();
};

#endif
