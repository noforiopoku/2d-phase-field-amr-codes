#! /usr/bin/env python
import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
from numpy import arange

#input parameters
first_num = raw_input('first?\n')
last_num  = raw_input('last?\n')
step_num = 0
if(first_num != last_num):
	step_num = raw_input('step?\n')
#uniform plot grid parameters
x_initial=0
x_final=1000
npoints_x=2500 
y_initial=0
y_final=1000
npoints_y=2500
#flags 1=Yes, 0=No
flag_firstline=1 	#ignore first line? 
flag_grid_plot=1 	#Plot grid?
flag_p_plot=1 		#Plot p?
flag_c_b_plot=1		#Plot cb?
flag_c_c_plot=1 	#Plot cc?

flag_c_bc_y0=0		#printout C_b, C_c on y=0 

# output figures size (inches)
h=7
w=h*(x_final-x_initial)/(y_final-y_initial)

#Uniform plot grid
xi = np.linspace(x_initial,x_final,npoints_x)
yi = np.linspace(y_initial,y_final,npoints_y)

if(int(step_num)==0):
    step_num=int(last_num)-int(first_num)+1 # step=0 does not work in range()

for num in range(int(first_num),int(last_num)+1,int(step_num)):

	#read data from file
	in_num=str(num)
	infile='aaa'+in_num+'.dat'
	print '\n',in_num
	x=[]; y=[]; p=[]; c_b=[]; c_c=[]
	with open(infile,'r') as fin:
		if (flag_firstline==1):
			nnodes=fin.readline()
                        nnodes=fin.readline()
		read_line = lambda: fin.readline()
		for line in iter(read_line,''):
			if (len(line.split())>=5):
				x.append(line.split()[0])
				y.append(line.split()[1])
				p.append(line.split()[2]) #p=sum(phi's)+ng-1
				c_b.append(line.split()[3])
				c_c.append(line.split()[4])

	x=np.float64(x)
	y=np.float64(y)

	if(flag_grid_plot==1):
	# plot original grid (original data points)
		fig=plt.figure(figsize=(w,h))
		fig_ax=fig.add_axes([0,0,1,1],frame_on=False)
  		plt.scatter(x,y,marker='o',edgecolors='none',c='black',s=1)
		plt.xlim(x_initial,x_final)
  		plt.ylim(y_initial,y_final)
  		plt.axis('off')
		fg_plot="grid"+in_num+".png"
		plt.savefig(fg_plot)
		plt.close()

	if(flag_p_plot==1):
		# grid and plot p, pad_inches = 0
		fig=plt.figure(figsize=(w,h))
		fig_ax=fig.add_axes([0,0,1,1],frame_on=False)
		zi=griddata((x,y),p,(xi[None,:],yi[:,None]),method='linear')
		plt.contourf(xi,yi,zi,100,cmap=plt.cm.jet)
		plt.axis('off')
		fg_plot="p"+in_num+".png"
		plt.savefig(fg_plot)
		plt.close()

	if(flag_c_b_plot==1):
		# grid and plot c
		fig=plt.figure(figsize=(w,h))
		fig_ax=fig.add_axes([0,0,1,1],frame_on=False)
		zi=griddata((x,y),c_b,(xi[None,:],yi[:,None]),method='linear')
		plt.contourf(xi,yi,zi,100,cmap=plt.cm.jet)
		plt.axis('off')
    	fg_plot="c_b"+in_num+".png"
    	plt.savefig(fg_plot)
    	plt.close()

	if(flag_c_c_plot==1):
		# grid and plot c
		fig=plt.figure(figsize=(w,h))
		fig_ax=fig.add_axes([0,0,1,1],frame_on=False)
		zi=griddata((x,y),c_c,(xi[None,:],yi[:,None]),method='linear')
		plt.contourf(xi,yi,zi,100,cmap=plt.cm.jet)
		plt.axis('off')
    	fg_plot="c_c"+in_num+".png"
    	plt.savefig(fg_plot)
    	plt.close()

	if(flag_c_bc_y0==1):
		#printout C_b on y=0 
		with open('cbx'+in_num+'.out','w') as fcbx:
			for i in range (0, len(c_b)):
				if float(y[i])==500:
					fcbx.write(str(x[i])+'\t'+str(c_b[i])+'\n')
		#printout C_c on y=0 
		with open('ccx'+in_num+'.out','w') as fccx:
			for i in range (0, len(c_c)):
				if float(y[i])==500:
					fccx.write(str(x[i])+'\t'+str(c_c[i])+'\n')

	print 'max(c_b)=',max(c_b)
	print 'max(c_c)=',max(c_c)
print






