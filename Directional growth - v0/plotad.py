#! /usr/bin/env python
import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
from numpy import arange

#input parameters
firstline=1 #ignore first line? 1=Yes, 0=No
filen = raw_input('filename?\n')
first_num = raw_input('first?\n')
last_num  = raw_input('last?\n')
step_num = raw_input('step?\n')

#uniform plot grid parameters
x_initial = int(raw_input('x_initial?\n')) 	#x_initial=0
x_final = int(raw_input('x_final?\n')) 		#x_final=512
npoints_x = int(raw_input('npoints_x?\n')) 	#npoints_x=1000 
y_initial = int(raw_input('y_initial?\n')) 	#y_initial=0
y_final = int(raw_input('y_final?\n')) 		#y_final=512
npoints_y = int(raw_input('npoints_y?\n')) 	#npoints_y=1000

#flags 1=Yes, 0=No
flag_firstline=1 	#ignore first line? 
flag_grid_plot=1 	#Plot grid?
flag_p_plot=1 		#Plot p?
flag_c_plot=1		#Plot c?
# output figures size (inches)
h=7
w=h*(x_final-x_initial)/(y_final-y_initial)

#Uniform plot grid
xi = np.linspace(x_initial,x_final,npoints_x)
yi = np.linspace(y_initial,y_final,npoints_y)

if(int(step_num)==0):
    step_num=int(last_num)-int(first_num)+1 # step=0 does not work in range()

for num in range(int(first_num),int(last_num)+1,int(step_num)):

	#read data from file
	in_num=str(num)
	infile=filen+in_num+'.dat'
	print '\n',in_num
	x=[]; y=[]; p=[]; c=[];
	with open(infile,'r') as fin:
		if (flag_firstline==1):
			nnodes=fin.readline()
			nnodes=fin.readline()
			nnodes=fin.readline()
			nnodes=fin.readline()
			nnodes=fin.readline()
			nnodes=fin.readline()
			nnodes=fin.readline()
			nnodes=fin.readline()
		read_line = lambda: fin.readline()
		for line in iter(read_line,''):
			x.append(line.split()[0])
			y.append(line.split()[1])
			p.append(line.split()[2]) #p=sum(phi's)+ng-1
			c.append(line.split()[3])

	if(flag_grid_plot==1):
	# plot original grid (original data points)
		fig=plt.figure(figsize=(w,h))
		fig_ax=fig.add_axes([0,0,1,1],frame_on=False)
  		plt.scatter(x,y,marker='o',edgecolors='none',c='black',s=1)
		plt.xlim(x_initial,x_final)
  		plt.ylim(y_initial,y_final)
  		plt.axis('off')
		fg_plot="grid"+in_num+".png"
		plt.savefig(fg_plot)
		plt.close()

	if(flag_p_plot==1):
		# grid and plot p, pad_inches = 0
		fig=plt.figure(figsize=(w,h))
		fig_ax=fig.add_axes([0,0,1,1],frame_on=False)
		zi=griddata((x,y),p,(xi[None,:],yi[:,None]),method='linear')
		plt.contourf(xi,yi,zi,100,cmap=plt.cm.jet)
		plt.axis('off')
		fg_plot="p"+in_num+".png"
		plt.savefig(fg_plot)
		plt.close()

	if(flag_c_plot==1):
		# grid and plot c
		fig=plt.figure(figsize=(w,h))
		fig_ax=fig.add_axes([0,0,1,1],frame_on=False)
		zi=griddata((x,y),c,(xi[None,:],yi[:,None]),method='linear')
		plt.contourf(xi,yi,zi,100,cmap=plt.cm.jet)
		plt.axis('off')
    	fg_plot="c"+in_num+".png"
    	plt.savefig(fg_plot)
    	plt.close()

	print
