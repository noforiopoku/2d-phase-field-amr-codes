#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "initial.h"
#include "grid.h"
#include <omp.h>

#include <cstdlib>
#include <climits>


using namespace std;

grid::~grid()
{
	delete [] nodeArray;
}

grid::grid(int Periodic)
{
	int i,j,q,l,c,k;
	int ii,jj;
	
	tmpnodes = new list<node*>;
	nodes = new list<node*>;
	ghosts = new list<node*>;
	nodeNULL = nodes->end();
	ghostNULL = ghosts->end();
	
	nodeArray = new arrayNode[12000000];
	
	AdaptiveTree = new tree*[1];
	nTrees = 1;

	node ***initNodes;
	treenode ***initEle1,***initEle0;

	//setting up the global orientations of each order parameter
	if( oriFlag == 0 )
	{
		for(ii=0;ii<ng;ii++)
			ang[ii] = 2.*Pi/(2.*sym)*(2.*(double)rand_r(&seed)/(double)RAND_MAX - 1);
	}
	else
	{
		for(ii=0;ii<ng;ii++)
			ang[ii] = ii*2.*Pi/(2.*sym*ng);

	}

	//setting up the global orientations and misorientation table
	for (ii=0;ii<ng;ii++)
	{
		psi[ii] = & dumypsi[ii*ng];
	}
	
	for (jj=0;jj<ng;jj++)
	{
		for(ii=0;ii<ng;ii++)
		{
			psi[ii][jj] = fabs( fabs(ang[ii]) - fabs(ang[jj]) );
		}
	}

	if(Periodic == 1) // Periodic Boundaries
	{
		/******************** DOMAIN ****************************/

		initNodes = new node**[Numx];
		for(i=0;i<Numx;i++)
			initNodes[i] = new node*[Numy];
	
		initEle0 = new treenode**[nx];
		for(i=0;i<nx;i++)
			initEle0[i] = new treenode*[ny];

		initEle1 = new treenode**[nx*2];
		for(i=0;i<nx*2;i++)
			initEle1[i] = new treenode*[ny*2];

		/********** Create Nodes ***********/
		for(i=0;i<Numx;i++)
		{
			for(j=0;j<Numy;j++)
			{
				initNodes[i][j] = new node(i*w/4,j*w/4,nodeNULL,ghostNULL);
				initNodes[i][j]->me = nodeNULL;
				initNodes[i][j]->gme = ghostNULL;
			}
		}

		/*********** Set Node Neighbours **********/
	
		//4 Point Neighbours

		for(i=0;i<Numx;i+=2)
		{
			for(j=0;j<Numy;j+=2)
			{
				initNodes[i][j]->NN[0] = initNodes[i][(Numy+j+2)%Numy];
				initNodes[i][j]->NN[1] = initNodes[(Numx+i+2)%Numx][j];
				initNodes[i][j]->NN[2] = initNodes[i][(Numy+j-2)%Numy];
				initNodes[i][j]->NN[3] = initNodes[(Numx+i-2)%Numx][j];
			}
		}

		/*********** Create Elements **************/
		c=0;
		for(i=0;i<nx;i++)  // Create Zeroth Level Elements
		{
			for(j=0;j<ny;j++)
			{
				initEle0[i][j] = new treenode(NULL,c,0);
				c++;
			}
		}

		for(i=0;i<nx*2;i++)//Create First Level Elements
		{
			for(j=0;j<ny*2;j++)
			{
				initEle1[i][j] = new treenode(initEle0[i/2][j/2],c,1);
				c++;
			}
		}
		for(i=0;i<nx;i++)	// Set Element number
		{
			for(j=0;j<ny;j++)
			{
				initEle1[i*2][j*2]->num = 3;
				initEle1[i*2+1][j*2]->num = 2;
				initEle1[i*2+1][j*2+1]->num = 1;
				initEle1[i*2][j*2+1]->num = 0;
			}
		}
		/***************** Apply Nodes To Elements ***************/

		for(i=0;i<nx;i++)
		{
			for(j=0;j<ny;j++)
			{
				q = 2*i;
				l = 2*j;
				initEle0[i][j]->N[0] = initNodes[(q)*2][((l+2)*2)%Numy];
				initEle0[i][j]->N[1] = initNodes[(q+1)*2][((l+2)*2)%Numy];
				initEle0[i][j]->N[2] = initNodes[((q+2)*2)%Numx][((l+2)*2)%Numy];
				initEle0[i][j]->N[3] = initNodes[(q)*2][(l+1)*2];
				initEle0[i][j]->N[4] = initNodes[(q+1)*2][(l+1)*2];
				initEle0[i][j]->N[5] = initNodes[((q+2)*2)%Numx][(l+1)*2];
				initEle0[i][j]->N[6] = initNodes[(q)*2][l*2];
				initEle0[i][j]->N[7] = initNodes[(q+1)*2][l*2];
				initEle0[i][j]->N[8] = initNodes[((q+2)*2)%Numx][l*2];
			}
		}
		for(i=0;i<nx*2;i++)
		{
			for(j=0;j<ny*2;j++)
			{
				q = 2*i;
				l = 2*j;
				initEle1[i][j]->N[0] = initNodes[q][(l+2)%Numy];
				initEle1[i][j]->N[1] = initNodes[q+1][(l+2)%Numy];
				initEle1[i][j]->N[2] = initNodes[(q+2)%Numx][(l+2)%Numy];
				initEle1[i][j]->N[3] = initNodes[q][l+1];
				initEle1[i][j]->N[4] = initNodes[q+1][l+1];
				initEle1[i][j]->N[5] = initNodes[(q+2)%Numx][l+1];
				initEle1[i][j]->N[6] = initNodes[q][l];
				initEle1[i][j]->N[7] = initNodes[q+1][l];
				initEle1[i][j]->N[8] = initNodes[(q+2)%Numx][l];	
			}
		}
		/********************* Element Neighbours **************************/

		for(i=0;i<nx;i++)
		{
			for(j=0;j<ny;j++)
			{
				initEle0[i][j]->EN[0] = initEle0[i][(ny+j+1)%ny];
				initEle0[i][j]->EN[1] = initEle0[(nx+i+1)%nx][j];
				initEle0[i][j]->EN[2] = initEle0[i][(ny+j-1)%ny];
				initEle0[i][j]->EN[3] = initEle0[(nx+i-1)%nx][j];
			}
		}

		for(i=0;i<2*nx;i++)
		{
			for(j=0;j<2*ny;j++)
			{
				initEle1[i][j]->EN[0] = initEle1[i][(2*ny+j+1)%(2*ny)];
				initEle1[i][j]->EN[1] = initEle1[(2*nx+i+1)%(2*nx)][j];
				initEle1[i][j]->EN[2] = initEle1[i][(2*ny+j-1)%(2*ny)];
				initEle1[i][j]->EN[3] = initEle1[(2*nx+i-1)%(2*nx)][j];
			}
		}

		/********************** Set Element Root level Children **************************/

		for(i=0;i<nx;i++)
		{
			for(j=0;j<ny;j++)
			{
				initEle0[i][j]->TN[0] = initEle1[i*2][j*2+1];
				initEle0[i][j]->TN[1] = initEle1[i*2+1][j*2+1];
				initEle0[i][j]->TN[2] = initEle1[i*2+1][j*2];
				initEle0[i][j]->TN[3] = initEle1[i*2][j*2];
			}
		}
		/*************** Create Tree ***********************/

		AdaptiveTree[0] = new tree(initEle0,initEle1,initNodes,nodes,ghosts);
	}
	else // Non-Periodic
	{
		/******************** DOMAIN ****************************/
		cout << "Create Domain" << endl;
		initNodes = new node**[2*nx+1];
		for(i=0;i<2*nx+1;i++)
			initNodes[i] = new node*[2*ny+1];
		
		initEle0 = new treenode**[nx];
		for(i=0;i<nx;i++)
			initEle0[i] = new treenode*[ny];

		/********** Create Nodes ***********/
		cout << "Create Nodes" << endl;
		for(i=0;i<2*nx+1;i++)
		{
			for(j=0;j<2*ny+1;j++)
			{
				initNodes[i][j] = new node(i*w/2.0,j*w/2.0,nodeNULL,ghostNULL);
				initNodes[i][j]->me = nodeNULL;
				initNodes[i][j]->gme = ghostNULL;
			}
		}
		/*********** Set Node Neighbours **********/
		cout << "Set Node Neighbours" << endl;
		//4 Point Neighbours

		for(i=0;i<2*nx+1;i+=2)
		{
			for(j=0;j<2*ny+1;j+=2)
			{
				if(j == 2*ny)
					initNodes[i][j]->NN[0] = NULL;
				else
					initNodes[i][j]->NN[0] = initNodes[i][j+2];
				if(i == 2*nx)
					initNodes[i][j]->NN[1] = NULL;
				else
					initNodes[i][j]->NN[1] = initNodes[i+2][j];
				if(j == 0)
					initNodes[i][j]->NN[2] = NULL;
				else
					initNodes[i][j]->NN[2] = initNodes[i][j-2];
				if(i == 0)
					initNodes[i][j]->NN[3] = NULL;
				else
					initNodes[i][j]->NN[3] = initNodes[i-2][j];
			}
		}
					
		for(i=0;i<2*nx+1;i+=2)
		{
			for(j=0;j<2*ny+1;j+=2)
			{
				if(i==0 && j==2*ny)//Top left
				{
					for(k=2;k<5;k++)
					{
						for(l=0;l<3;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}
				}
				else if(i==2*nx && j==2*ny)//Top Right
				{
					for(k=0;k<3;k++)
					{
						for(l=0;l<3;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}
				}
				else if(i==2*nx && j==0)//Bottom Right
				{
					for(k=0;k<3;k++)
					{
						for(l=2;l<5;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}
				}
				else if(i==0 && j ==0)//Bottom Left
				{
					for(k=2;k<5;k++)
					{
						for(l=2;l<5;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}
				}
				else if(i==0)//Left
				{
					for(k=2;k<5;k++)
					{
						for(l=0;l<5;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}	
				}
				else if(j==2*ny)//Top
				{
					for(k=0;k<5;k++)
					{
						for(l=0;l<3;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}						
				}
				else if(i==2*nx)//Right
				{
					for(k=0;k<3;k++)
					{
						for(l=0;l<5;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}					
				}
				else if(j==0)//Bottom
				{
					for(k=0;k<5;k++)
					{
						for(l=2;l<5;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}						
				}
				else//Core
				{
					for(k=0;k<5;k++)
					{
						for(l=0;l<5;l++)
						{
							initNodes[i][j]->N25[k][l] = initNodes[i-2+k][j-2+l];
						}
					}
				}
				
			}
		}
		
		

		/*********** Create Elements **************/
		cout << "Create Elements" << endl;
		c=0;
		for(i=0;i<nx;i++)  // Create Zeroth Level Elements
		{
			for(j=0;j<ny;j++)
			{
				initEle0[i][j] = new treenode(NULL,c,0);
				c++;
			}
		}

		/***************** Apply Nodes To Elements ***************/

		cout << "Apply Nodes to Elements" << endl;
		for(i=0;i<nx;i++)
		{
			for(j=0;j<ny;j++)
			{
				q = 2*i;
				l = 2*j;
				initEle0[i][j]->N[0] = initNodes[q][l+2];
				initEle0[i][j]->N[1] = initNodes[q+1][l+2];
				initEle0[i][j]->N[2] = initNodes[q+2][l+2];
				initEle0[i][j]->N[3] = initNodes[q][l+1];
				initEle0[i][j]->N[4] = initNodes[q+1][l+1];
				initEle0[i][j]->N[5] = initNodes[q+2][l+1];
				initEle0[i][j]->N[6] = initNodes[q][l];
				initEle0[i][j]->N[7] = initNodes[q+1][l];
				initEle0[i][j]->N[8] = initNodes[q+2][l];
			}
		}
		
		/********************* Element Neighbours **************************/
		cout << "Set Element Neighbours" << endl;
		for(i=0;i<nx;i++)
		{
			for(j=0;j<ny;j++)
			{
				if(j==ny-1)
					initEle0[i][j]->EN[0] = NULL;
				else
					initEle0[i][j]->EN[0] = initEle0[i][j+1];
				if(i==nx-1)
					initEle0[i][j]->EN[1] = NULL;
				else
					initEle0[i][j]->EN[1] = initEle0[i+1][j];
				if(j==0)
					initEle0[i][j]->EN[2] = NULL;
				else
					initEle0[i][j]->EN[2] = initEle0[i][j-1];
				if(i==0)
					initEle0[i][j]->EN[3] = NULL;
				else
					initEle0[i][j]->EN[3] = initEle0[i-1][j];
			}
		}
		

		/********************** Set Element Root level Children **************************/
		cout << "Set Element Children" << endl;
		for(i=0;i<nx;i++)
		{
			for(j=0;j<ny;j++)
			{
				initEle0[i][j]->TN[0] = NULL;
				initEle0[i][j]->TN[1] = NULL;
				initEle0[i][j]->TN[2] = NULL;
				initEle0[i][j]->TN[3] = NULL;
			}
		}
		/*************** Create Tree ***********************/
		cout << "Create Tree" << endl;
		AdaptiveTree[0] = new tree(initEle0,initNodes,nodes,ghosts);
	}
	/*************** Set Node Size ********************/
	nodesize = (int)nodes->size();
	ghostsize=0;
}

void grid::initializeGrid()
{
	int trees;
	
	for(trees=0;trees<nTrees;trees++)
    	AdaptiveTree[trees]->initialize(nodes,ghosts);

	nodesize = (int)nodes->size();
	ghostsize = (int)ghosts->size();
}

void grid::setdx()
{
	int trees;
	for(trees=0;trees<nTrees;trees++)
	    AdaptiveTree[trees]->setdx(w);
}

void grid::updateGrid(int npt)
{
	int i,trees;
	int ii;
	
	list<node*>::iterator it;
	for(it = nodes->begin();it != nodes->end();it++)
	{
	    i = (*it)->index;
		
		for(ii=0;ii<npt;ii++)
			(*it)->Phi[ii] = nodeArray[i].Phi[ii];

		(*it)->C = nodeArray[i].C;
	    (*it)->T = nodeArray[i].T;
	}
	
	for(it = ghosts->begin();it != ghosts->end();it++)
	{
		i = (*it)->index;

		for(ii=0;ii<npt;ii++)
			(*it)->Phi[ii] = nodeArray[i].Phi[ii];
        
		(*it)->C = nodeArray[i].C;
	    (*it)->T = nodeArray[i].T;
	}
	
	for(trees=0;trees<nTrees;trees++)
		AdaptiveTree[trees]->regrid(nodes, ghosts);

	nodesize = (int)nodes->size();
	ghostsize = (int)ghosts->size();
}

void grid::createArray(int npt)
{
	int i,j;
	int B[4];
	int Bsize;
	int ii;

	list<node*>::iterator it;
	list<node*>::iterator it2;
	list<node*>::iterator itmp;
	
	for(i=0;i<4;i++)
		B[i]=0;

	/******************COPY LIST***************************/
	i = 0;
	for(it = nodes->begin();it != nodes->end();it++)
	{
		for(ii=0;ii<npt;ii++)
		{
			nodeArray[i].Phi[ii] = (*it)->Phi[ii];						
		}
        
		nodeArray[i].C = (*it)->C;
		nodeArray[i].T = (*it)->T;
	    nodeArray[i].x = (*it)->x;
	    nodeArray[i].y = (*it)->y;
	    nodeArray[i].dx = (*it)->dx;			
	    (*it)->index = i;
	    i++;
	}

	i = nodesize;
	for(it = ghosts->begin();it != ghosts->end();it++)
	{  
		for(ii=0;ii<npt;ii++)
		{
			nodeArray[i].Phi[ii] = (*it)->Phi[ii];						
		}			
		nodeArray[i].C= (*it)->C;
		nodeArray[i].T = (*it)->T;
		nodeArray[i].x = (*it)->x;
		nodeArray[i].y = (*it)->y;
		(*it)->index = i;
		i++;
	}


	/******************4 point neighbours***************************/
	for(it = nodes->begin();it != nodes->end();it++)
	{
	    for(i=0;i<4;i++)
		{
			if((*it)->NN[i] != NULL)
			{
				nodeArray[(*it)->index].NN[i] = (*it)->NN[i]->index;    
			}
			else
			{
				nodeArray[(*it)->index].NN[i] = -1;
			}
		}
		for(i=0;i<5;i++)
		{
			for(j=0;j<5;j++)
			{
				if((*it)->N25[i][j] != NULL)
				{
					nodeArray[(*it)->index].N25[i][j] = (*it)->N25[i][j]->index;
				}
				else
				{
					nodeArray[(*it)->index].N25[i][j] = -1;
				}
			}
		}
	}

	//Ghosts
	i=nodesize;
	for(it = ghosts->begin();it != ghosts->end();it++)
	{
	    nodeArray[i].NN[0] = (*it)->NN[0]->index;
	    nodeArray[i].NN[1] = (*it)->NN[1]->index;
	    if((*it)->NN[2] != NULL)
	    {
			nodeArray[i].NN[2] = (*it)->NN[2]->index;
			nodeArray[i].NN[3] = (*it)->NN[3]->index;
		}
		else
		{
			nodeArray[i].NN[2] = -1;
			nodeArray[i].NN[3] = -1;
		}
		i++;
	}
	
	// Boundaries
	/***********
	3-----0-----0
	|			|
	|			|
	|			|
	3			1
	|			|
	|			|
	|			|
	2-----2-----1
	**********/

	/******************Boundary Node Copy***************************/
	bt=4;
	b0 = 0;
	b1=0;
	b2=0;
	b3=0;
	Bsize = nodesize+ghostsize;
	/*********************** Boundary 0 *****************************/
	i=0;
	for(it = nodes->begin();it != nodes->end();it++)
	{
		
		if((*it)->NN[0] == NULL)
		{
			
			if((*it)->NN[1] != NULL && (*it)->NN[3] != NULL)
			{
				nodeArray[Bsize+bt+i].NN[2]  =(*it)->N25[1][1]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[2][1]->index;
				nodeArray[Bsize+bt+i+2].NN[2]=(*it)->N25[3][1]->index;
				nodeArray[(*it)->index].N25[1][3] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[2][3] = Bsize+bt+i+1;
				nodeArray[(*it)->index].N25[3][3] = Bsize+bt+i+2;
				nodeArray[(*it)->index].NN[0] = Bsize+bt+i+1;
				i+=3;
				b0+=3;
			}
			else if((*it)->NN[1] == NULL)
			{
				nodeArray[Bsize+bt+i].NN[2]=(*it)->N25[1][1]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[2][1]->index;
				nodeArray[Bsize].NN[2]=(*it)->N25[1][1]->index;
				nodeArray[(*it)->index].N25[1][3] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[2][3] = Bsize+bt+i+1;
				nodeArray[(*it)->index].N25[3][3] = Bsize;
				nodeArray[(*it)->index].NN[0] = Bsize+bt+i+1;
				i+=2;
				b0+=2;
			}
			else if((*it)->NN[3] == NULL)
			{
				nodeArray[Bsize+3].NN[2]=(*it)->N25[3][1]->index;
				nodeArray[Bsize+bt+i].NN[2]=(*it)->N25[2][1]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[3][1]->index;
				nodeArray[(*it)->index].N25[1][3] = Bsize+3;
				nodeArray[(*it)->index].N25[2][3] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[3][3] = Bsize+bt+i+1;
				nodeArray[(*it)->index].NN[0] = Bsize+bt+i;
				i+=2;
				b0+=2;
			}
		}	
	}
	
	/*********************** Boundary 1 *****************************/
	i=0;
	bt += b0;
	for(it = nodes->begin();it != nodes->end();it++)
	{
		if((*it)->NN[1] == NULL)
		{
		
			if((*it)->NN[0] != NULL && (*it)->NN[2] != NULL)
			{
				nodeArray[Bsize+bt+i].NN[2]  =(*it)->N25[1][1]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[1][2]->index;
				
				nodeArray[Bsize+bt+i+2].NN[2]=(*it)->N25[1][3]->index;
				nodeArray[(*it)->index].N25[3][1] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[3][2] = Bsize+bt+i+1;
				nodeArray[(*it)->index].N25[3][3] = Bsize+bt+i+2;
				nodeArray[(*it)->index].NN[1] = Bsize+bt+i+1;
				i+=3;
				b1+=3;
			}
			else if((*it)->NN[0] == NULL)
			{
				
				nodeArray[Bsize+bt+i].NN[2]=(*it)->N25[1][1]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[1][2]->index;
				nodeArray[(*it)->index].N25[3][1] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[3][2] = Bsize+bt+i+1;
				nodeArray[(*it)->index].NN[1] = Bsize+bt+i+1;
				i+=2;
				b1+=2;
			}
			else if((*it)->NN[2] == NULL)
			{
				
				nodeArray[Bsize+bt+i].NN[2]=(*it)->N25[1][2]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[1][3]->index;
				nodeArray[(*it)->index].N25[3][2] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[3][3] = Bsize+bt+i+1;
				nodeArray[(*it)->index].NN[1] = Bsize+bt+i;
				i+=2;
				b1+=2;
			}
		}
	}
	
	/*********************** Boundary 2 *****************************/
	i=0;
	bt+=b1;
	for(it = nodes->begin();it != nodes->end();it++)
	{
		if((*it)->NN[2] == NULL)
		{
			if((*it)->NN[1] != NULL && (*it)->NN[3] != NULL)
			{
				nodeArray[Bsize+bt+i].NN[2]  =(*it)->N25[1][3]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[2][3]->index;
				nodeArray[Bsize+bt+i+2].NN[2]=(*it)->N25[3][3]->index;
				nodeArray[(*it)->index].N25[1][1] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[2][1] = Bsize+bt+i+1;
				nodeArray[(*it)->index].N25[3][1] = Bsize+bt+i+2;
				nodeArray[(*it)->index].NN[2] = Bsize+bt+i+1;
				i+=3;
				b2+=3;
			}
			else if((*it)->NN[1] == NULL)
			{
				nodeArray[Bsize+bt+i].NN[2]=(*it)->N25[1][3]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[2][3]->index;
				nodeArray[Bsize+1].NN[2]=(*it)->N25[1][3]->index;
				nodeArray[(*it)->index].N25[1][1] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[2][1] = Bsize+bt+i+1;
				nodeArray[(*it)->index].N25[3][1] = Bsize+1;
				nodeArray[(*it)->index].NN[2] = Bsize+bt+i+1;
				i+=2;
				b2+=2;
			}
			else if((*it)->NN[3] == NULL)
			{
				nodeArray[Bsize+2].NN[2]=(*it)->N25[3][3]->index;
				nodeArray[Bsize+bt+i].NN[2]=(*it)->N25[2][3]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[3][3]->index;
				nodeArray[(*it)->index].N25[1][1] = Bsize+2;
				nodeArray[(*it)->index].N25[2][1] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[3][1] = Bsize+bt+i+1;
				nodeArray[(*it)->index].NN[2] = Bsize+bt+i;
				i+=2;
				b2+=2;
			}
		}
	}
	
	/*********************** Boundary 3 *****************************/
	i=0;
	bt+=b2;
	for(it = nodes->begin();it != nodes->end();it++)
	{
		if((*it)->NN[3] == NULL)
		{
			if((*it)->NN[0] != NULL && (*it)->NN[2] != NULL)
			{
				nodeArray[Bsize+bt+i].NN[2]  =(*it)->N25[3][1]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[3][2]->index;
				nodeArray[Bsize+bt+i+2].NN[2]=(*it)->N25[3][3]->index;
				nodeArray[(*it)->index].N25[1][1] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[1][2] = Bsize+bt+i+1;
				nodeArray[(*it)->index].N25[1][3] = Bsize+bt+i+2;
				nodeArray[(*it)->index].NN[3] = Bsize+bt+i+1;
				i+=3;
				b3+=3;
			}
			else if((*it)->NN[0] == NULL)
			{
				nodeArray[Bsize+bt+i].NN[2]=(*it)->N25[3][1]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[3][2]->index;
				nodeArray[(*it)->index].N25[1][1] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[1][2] = Bsize+bt+i+1;
				nodeArray[(*it)->index].NN[3] = Bsize+bt+i+1;

				i+=2;
				b1+=2;
			}
			else if((*it)->NN[2] == NULL)
			{
				nodeArray[Bsize+bt+i].NN[2]=(*it)->N25[3][2]->index;
				nodeArray[Bsize+bt+i+1].NN[2]=(*it)->N25[3][3]->index;
				nodeArray[(*it)->index].N25[1][2] = Bsize+bt+i;
				nodeArray[(*it)->index].N25[1][3] = Bsize+bt+i+1;
				nodeArray[(*it)->index].NN[3] = Bsize+bt+i;
			
				i+=2;
				b1+=2;
			}

		}
	}
}
  
void grid::findNucSites(double& xnuc, double& ynuc, double& radnuc, double& Cnuc, int& fnuc, int& nucflag, int npt, int niter, double *spx, double *spy, double *spz, int *ocv)
{
	int trees;
	int numSites=0;
	
	//Calculating Number of in liquid
	for(trees=0;trees<nTrees;trees++)
	{
		numSites = AdaptiveTree[trees]->findNucSites(xnuc,ynuc,radnuc,Cnuc,fnuc,npt,niter,spx,spy,spz,ocv);
	}

	if ( numSites > 0 )
		nucflag = 1;
}
    
void grid::nucleate(double xnuc, double ynuc, double radnuc, double Cnuc, int fnuc)
{
	int trees;
	
	for(trees=0;trees<nTrees;trees++)
	    AdaptiveTree[trees]->nucleate(nodes,ghosts,xnuc,ynuc,radnuc,Cnuc,fnuc);

	nodesize = (int)nodes->size();
	ghostsize = (int)ghosts->size();
}

void grid::updateGhosts(int l,int npt)
{
	int i,j;
	int ii;

	double Phi[ng],C;
	double A[ng],dA[ng],dPdt[ng],dPhix[ng],dPhiy[ng];
	double Cx,Cy,eU,q;
	double u1,u2;
	double sumphi;
	double T;
	
	if(l == 1)
	{
		//begin openmp
		#pragma omp parallel default(shared) private(ii,Phi,C,j,T) num_threads(numprocs)
		{
			#pragma omp for schedule(static)
		    for(i=nodesize;i<nodesize+ghostsize;i++)
		    {
				C = 0.0;
				T = 0.0;
				for(ii=0;ii<npt;ii++)
				{
					Phi[ii] = 0.0;

					for(j=0;nodeArray[i].NN[j] != -1 && j<4;j++)
					{
						Phi[ii] += nodeArray[nodeArray[i].NN[j]].Phi[ii];
					}

					nodeArray[i].Phi[ii] = Phi[ii]/j;
				}
				
				for(j=0;nodeArray[i].NN[j] != -1 && j<4;j++)
				{					
					C += nodeArray[nodeArray[i].NN[j]].C;					
					T += nodeArray[nodeArray[i].NN[j]].T;					
				}

				nodeArray[i].C = C/j;				
				nodeArray[i].T = T/j;  
		    }
		}//end openmp
	}
	else if(l ==2)
	{
		//begin openmp
		#pragma omp parallel default(shared) private(ii,eU,q,A,dA,j,dPhix,dPhiy,sumphi) num_threads(numprocs)
	    {
			#pragma omp for schedule(static)				
			for(i=nodesize;i<nodesize+ghostsize;i++)
			{
				eU = 0.0;
				q = 0.0;
				sumphi = 0.0;

				for(ii=0;ii<npt;ii++)
				{
					A[ii] = 0.0;
					dA[ii] = 0.0;	
					dPhix[ii] = 0.0;
					dPhiy[ii] = 0.0;

					for(j=0;nodeArray[i].NN[j] != -1 && j<4;j++)
					{
						A[ii] += nodeArray[nodeArray[i].NN[j]].A[ii];
						dA[ii] += nodeArray[nodeArray[i].NN[j]].dA[ii];
						dPhix[ii] += nodeArray[nodeArray[i].NN[j]].dPhix[ii];
						dPhiy[ii] += nodeArray[nodeArray[i].NN[j]].dPhiy[ii];
					}

					nodeArray[i].A[ii] = A[ii]/j;
					nodeArray[i].dA[ii] = dA[ii]/j;
					nodeArray[i].dPhix[ii] = dPhix[ii]/j;
					nodeArray[i].dPhiy[ii] = dPhiy[ii]/j;
				}

				for(j=0;nodeArray[i].NN[j] != -1 && j<4;j++)
				{					
					eU += nodeArray[nodeArray[i].NN[j]].eU;
					q += nodeArray[nodeArray[i].NN[j]].q;
					sumphi += nodeArray[nodeArray[i].NN[j]].sumphi;
				}
				
				nodeArray[i].eU = eU/j;
				nodeArray[i].q = q/j;
				nodeArray[i].sumphi = sumphi/j;
			}
	    }//end openmp 	
	}
	else if(l ==3)
	{
		//begin openmp
		#pragma omp parallel default(shared) private(ii,j,dPdt) num_threads(numprocs)
	    {
			#pragma omp for schedule(static)				
			for(i=nodesize;i<nodesize+ghostsize;i++)
			{
				for(ii=0;ii<npt;ii++)
				{
					dPdt[ii] = 0.0;

					for(j=0;nodeArray[i].NN[j] != -1 && j<4;j++)
					{
						dPdt[ii] += nodeArray[nodeArray[i].NN[j]].dPdt[ii];
					}
					nodeArray[i].dPdt[ii] = dPdt[ii]/j;
				}
			}
	    }//end openmp 	
	}
	else if(l==4)
	{
		//begin openmp
		#pragma omp parallel default(shared) private(Cx,Cy,u1,u2,j) num_threads(numprocs)
	    {
			#pragma omp for schedule(static)
			for(i=nodesize;i<nodesize+ghostsize;i++)
			{
				Cx = 0.0;
				Cy = 0.0;
				u1 = 0.0;
				u2 = 0.0;
				
				for(j=0;nodeArray[i].NN[j] != -1 && j<4;j++)
				{
					Cx += nodeArray[nodeArray[i].NN[j]].Cx;
				    Cy += nodeArray[nodeArray[i].NN[j]].Cy;
					u1 += nodeArray[nodeArray[i].NN[j]].u1;
				    u2 += nodeArray[nodeArray[i].NN[j]].u2;
				}

				nodeArray[i].Cx = Cx/j;
				nodeArray[i].Cy = Cy/j;
				nodeArray[i].u1 = u1/j;
				nodeArray[i].u2 = u2/j;
			}  
		}//end openmp	
	}
}

void grid::calcprePhi(int npt)
{
	int t,n0,n1,n2,n3;
	double dPhix,dPhiy;
	double dx;

	double Psum,RS,sumNum,sumDenom,theta;
	double constMo;
	double rscld,rscld2;
	int ii,jj;
	
	//begin openmp
	#pragma omp parallel default(shared) private(t,n0,n1,n2,n3,dPhix,dPhiy,dx,Psum,RS,sumNum,\
	sumDenom,theta,constMo,rscld,rscld2,ii,jj) num_threads(numprocs)
	{
		#pragma omp for schedule(static)			
		for(t=0;t<nodesize;t++)	
		{
			n0 = nodeArray[t].NN[0];
			n1 = nodeArray[t].NN[1];
			n2 = nodeArray[t].NN[2];
			n3 = nodeArray[t].NN[3];

			dx = nodeArray[t].dx;

			Psum = 0.0;
			sumNum = 0.0;
			sumDenom = 0.0;
			constMo = 0.;

			for (jj=0;jj<npt;jj++)
			{
				rscld = 0.5*(nodeArray[t].Phi[jj]+1);
				Psum = Psum + nodeArray[t].Phi[jj];			

				for (ii=0;ii<npt;ii++)
				{
					rscld2 = 0.5*(nodeArray[t].Phi[ii]+1);
					if (ii != jj)
					{
						sumNum = sumNum + rscld*rscld*rscld2*rscld2*psi[ii][jj];
						sumDenom = sumDenom + rscld*rscld*rscld2*rscld2;
					}
				}
			}

			if (sumDenom > 1E-4)
			{
				constMo = sumNum / sumDenom;				
			}
			nodeArray[t].sumphi = Psum+npt-1.0;
			nodeArray[t].eU = 2*nodeArray[t].C*(1.0/(1.+k-(1.-k)*nodeArray[t].sumphi));
			nodeArray[t].q = (1-nodeArray[t].sumphi) / (1.+k-(1.-k)*nodeArray[t].sumphi) 
						   + (1+nodeArray[t].sumphi)*0.5*( Epsilon*exp(-Qe/(R*nodeArray[t].T)) ) / (1.+k-(1.-k)*nodeArray[t].sumphi)  ;
		
			dPhix = 0.0;
			dPhiy = 0.0;
			theta = 0.0;
			RS = 0.0;

			if(constMo < 1E-4)
				RS = 0.0;
			else
				RS = constMo/maxmo*(1-log(constMo/maxmo));

			for (ii=0;ii<npt;ii++)
			{
			
				dPhix = ( nodeArray[n1].Phi[ii] - nodeArray[n3].Phi[ii] ) / (2.*dx);
 				dPhiy = ( nodeArray[n0].Phi[ii] - nodeArray[n2].Phi[ii] ) / (2.*dx);

				nodeArray[t].dPhix[ii] = dPhix;
				nodeArray[t].dPhiy[ii] = dPhiy;

				if(dPhix*dPhix < 1E-8 || dPhiy*dPhiy < 1E-8)
				{
					nodeArray[t].A[ii] = 1+Es;
				    nodeArray[t].dA[ii] = 0.0;
				}
				else
				{
					theta = atan2(dPhiy,dPhix);

					if (sumDenom > 1E-4 && constMo < maxmo)
					{
						nodeArray[t].A[ii] = 1+Es*cos( sym*( theta - ang[ii] ) ) * RS;
						nodeArray[t].dA[ii] = -sym*Es*sin( sym*( theta - ang[ii] ) ) * RS;
					}
					else
					{
						nodeArray[t].A[ii] = 1+Es*cos( sym*( theta - ang[ii] ) );
						nodeArray[t].dA[ii] = -sym*Es*sin( sym*( theta - ang[ii] ) );
					}
				}
			}
		}
	}// end openmp
}

void grid::calcdPdt(int niter,int npt)
{
	int t,n0,n1,n2,n3;
	int pp,mm,pm,mp;
	double phi,AyAx,dx;
	double t1,t2,t3,t4,GradPhi;
	double tgrad,tau_phi;

	int ii,jj;
	double rscld,rscld2,phisqsum;

	//begin openmp
	#pragma omp parallel default(shared) private(t,n0,n1,n2,n3,pp,mm,pm,mp,phi,AyAx,dx,\
	t1,t2,t3,t4,GradPhi,tgrad,tau_phi,ii,jj,rscld,rscld2,phisqsum) num_threads(numprocs)
	{
		#pragma omp for schedule(static)
		for(t=0;t<nodesize;t++)
		{
			n0 = nodeArray[t].NN[0];
			n1 = nodeArray[t].NN[1];
			n2 = nodeArray[t].NN[2];
			n3 = nodeArray[t].NN[3];
			pp = nodeArray[t].N25[3][3];
			mm = nodeArray[t].N25[1][1];
			pm = nodeArray[t].N25[3][1];
			mp = nodeArray[t].N25[1][3];
			
			dx = nodeArray[t].dx;
           	                                     		
			for(ii=0;ii<npt;ii++)
			{
				phi = nodeArray[t].Phi[ii];

				GradPhi = 2.0/3.0*(nodeArray[n0].Phi[ii] +nodeArray[n1].Phi[ii] +nodeArray[n2].Phi[ii] +nodeArray[n3].Phi[ii] 
						+ 0.25*(nodeArray[pp].Phi[ii] +nodeArray[mp].Phi[ii] +nodeArray[pm].Phi[ii] +nodeArray[mm].Phi[ii] ) 
						- 5.0*nodeArray[t].Phi[ii] );				
				
				t1 = phi - phi*phi*phi;

				t2 = -L*(1./(1.-k))*( nodeArray[t].eU - 1. + (nodeArray[t].T - Tl)/(m*cl_o) )*(1-phi*phi)*(1-phi*phi);
				
				phisqsum=0.0;
				rscld = 0.5*(phi + 1);
				for (jj=0;jj<npt;jj++)
				{
					if (jj != ii)
					{
						rscld2 = 0.5*(nodeArray[t].Phi[jj] + 1);
						phisqsum = phisqsum + rscld2*rscld2;
					}
				}
				
				t3 = -2.0*obs*rscld*phisqsum;
				
				t4 = nodeArray[t].A[ii]/(dx*dx)*
	                            ( 0.5
									*(
										(nodeArray[n1].A[ii] - nodeArray[n3].A[ii])*(nodeArray[n1].Phi[ii] - nodeArray[n3].Phi[ii])
										+
				 				       	(nodeArray[n0].A[ii] - nodeArray[n2].A[ii])*(nodeArray[n0].Phi[ii] - nodeArray[n2].Phi[ii])
						  			)
									+ nodeArray[t].A[ii]*GradPhi );

				AyAx = ( nodeArray[n0].A[ii]*nodeArray[n0].dA[ii]*nodeArray[n0].dPhix[ii] - nodeArray[n2].A[ii]*nodeArray[n2].dA[ii]*nodeArray[n2].dPhix[ii] 
			   		   - nodeArray[n1].A[ii]*nodeArray[n1].dA[ii]*nodeArray[n1].dPhiy[ii] + nodeArray[n3].A[ii]*nodeArray[n3].dA[ii]*nodeArray[n3].dPhiy[ii] ) / (2.*dx);
				
				tau_phi = (nodeArray[t].A[ii]*nodeArray[t].A[ii])*(1.-(1.-k)*tgrad);
				nodeArray[t].dPdt[ii] = (t1 + t2 + t3 + t4 + AyAx)/tau_phi;
			}
		}
	}// end openmp
}
			
void grid::calcUnoise()
{
	int t;
	double dx;	
	double v1,v2,rsqd,r,xgau,ygau,qeU;

	//begin openmp
	#pragma omp parallel default(shared) private(t,dx,v1,v2,rsqd,r,xgau,ygau,qeU) num_threads(numprocs)
	{
		#pragma omp for schedule(static)
		for(t=0;t<nodesize;t++)	
		{
			
			dx = nodeArray[t].dx;
			qeU = 0.5*(1.-nodeArray[t].sumphi)*nodeArray[t].eU;
			
			do
			{
				v1=2.0*( double(rand_r(&seed))/double(RAND_MAX) ) - 1.0;
				v2=2.0*( double(rand_r(&seed))/double(RAND_MAX) ) - 1.0;
				
				rsqd=v1*v1+v2*v2;
			} while ( rsqd>=1.0 );
			
			r=sqrt(-2.0*log(rsqd)/rsqd);
			xgau = v1*r;
			ygau = v2*r;
			
			nodeArray[t].u1 = sqrt(2*D*qeU*Fu_dt/(dx*dx))*xgau;
			nodeArray[t].u2 = sqrt(2*D*qeU*Fu_dt/(dx*dx))*ygau;		
		}
	}//end openmp
}

void grid::calcpreC(int npt)
{
	int t,n0,n1,n2,n3;
	double dx;
	double q,eu,eU;
	double deUx,deUy;
	double dphix,dphiy,dPdt;
	double magdP,sumdPdtNormx,sumdPdtNormy;

	int ii;

	//begin openmp
	#pragma omp parallel default(shared) private(t,n0,n1,n2,n3,dx,q,eu,eU,deUx,deUy,dphix,dphiy,\
	dPdt,magdP,sumdPdtNormx,sumdPdtNormy,ii) num_threads(numprocs)
	{
		#pragma omp for schedule(static)					
		for(t=0;t<nodesize;t++)	
		{
			n0 = nodeArray[t].NN[0];
			n1 = nodeArray[t].NN[1];
			n2 = nodeArray[t].NN[2];
			n3 = nodeArray[t].NN[3];

			dx = nodeArray[t].dx;

			q = nodeArray[t].q;
			eu = .5*(1.+k-(1.-k)*nodeArray[t].sumphi);
			eU = nodeArray[t].eU;
			
			deUx = .5*( nodeArray[n1].eU - nodeArray[n3].eU ) / dx;
			deUy = .5*( nodeArray[n0].eU - nodeArray[n2].eU ) / dx;

			sumdPdtNormx = 0;
			sumdPdtNormy = 0;
			for(ii=0;ii<npt;ii++)
			{
				dphix = nodeArray[t].dPhix[ii];
				dphiy = nodeArray[t].dPhiy[ii];
				dPdt = nodeArray[t].dPdt[ii];

				magdP = sqrt( dphix*dphix + dphiy*dphiy );

				if( magdP > 1E-8 )
				{
					sumdPdtNormx = sumdPdtNormx + dPdt*dphix/magdP;
					sumdPdtNormy = sumdPdtNormy + dPdt*dphiy/magdP;
				}
				else
				{
					sumdPdtNormx = sumdPdtNormx + 0.;
					sumdPdtNormy = sumdPdtNormy + 0.;
				}
			}

			nodeArray[t].Cx = D*q*eu*deUx + at*(1.-k)*eU*sumdPdtNormx;
			nodeArray[t].Cy = D*q*eu*deUy + at*(1.-k)*eU*sumdPdtNormy;
		}			
	}//end openmp		
}

void grid::calcdCdt(int niter, int npt)
{
	int t;
	int n0,n1,n2,n3;
	double dCx,dCy;
	double dx,dnoise;

	//begin openmp
	#pragma omp parallel default(shared) private(t,n0,n1,n2,n3,dCx,dCy,dx,dnoise) num_threads(numprocs)
	{
		#pragma omp for schedule(static)
		for(t=0;t<nodesize;t++)
		{
			n0 = nodeArray[t].NN[0];
			n1 = nodeArray[t].NN[1];
			n2 = nodeArray[t].NN[2];
			n3 = nodeArray[t].NN[3];

			dx = nodeArray[t].dx;

			dCx = .5*( nodeArray[n1].Cx - nodeArray[n3].Cx ) / dx;
			dCy = .5*( nodeArray[n0].Cy - nodeArray[n2].Cy ) / dx;

			dnoise = (nodeArray[n1].u1 - nodeArray[t].u1 + nodeArray[n0].u2 - nodeArray[t].u2)/dx;
		
			nodeArray[t].dCdt = ( dCx + dCy + dnoise );
		}
	}//end openmp
}

void grid::step(int npt)
{
	int t;

	//begin openmp
	#pragma omp parallel default(shared) private(t) num_threads(numprocs)
	{
		#pragma omp for schedule(static)
		for(t=0;t<nodesize;t++)	
		{
			nodeArray[t].updatePhi(npt);
			nodeArray[t].updateC();			
		}
	}//end openmp	
}
	
void grid::output(int l,int npt)
{
	int t,ii;
	double Psum;
	char filename[BUFSIZ];
	FILE *fp;
    
	sprintf(filename,"./aaa%d.dat",l);
	fp = fopen(filename,"w");

	/********************** ADAPTIVE OUTPUT *******************************/
	fprintf(fp,"%d\n",ng);
	
	for (ii=0;ii<ng;ii++)
		fprintf(fp,"%1.16f\n",ang[ii]);

	fprintf(fp,"%d\n",npt);
	fprintf(fp,"%d\n",nodesize);
	for(t=0;t<nodesize;t++)	
	{
		Psum=0.0;
		for (ii=0;ii<npt;ii++)
			Psum = Psum + nodeArray[t].Phi[ii];
                    
        fprintf(fp,"%1.8f %1.8f %1.8f %1.8f ", nodeArray[t].x, nodeArray[t].y, Psum+npt-1., nodeArray[t].C);

        for (ii=0;ii<npt;ii++)
        	if( ii == npt-1 )
				fprintf(fp,"%1.8f\n",nodeArray[t].Phi[ii] );
			else
				fprintf(fp,"%1.8f ",nodeArray[t].Phi[ii] );
	}
	fclose(fp);	
}
   
void grid::outputelement(int l,int npt)
{
	int i,j,esize;
	double *elements;
	char Efile[BUFSIZ];
	FILE *efp;
	sprintf(Efile,"./ele%d.dat",l);
	efp = fopen(Efile,"w");
	esize = AdaptiveTree[0]->getElementNum();
	elements = new double[esize*20];
	AdaptiveTree[0]->PackElements(elements,ang,npt);

	for(i=0;i<esize*20;i+=20)
	{
		for(j=0;j<20;j++)
		{
			fprintf(efp,"%f   \n",elements[i+j]);
		}
		
	}
	delete elements;
	fclose(efp);
}

void grid::updateBC(int npt)
{
	int i,j,k;

	//begin openmp
	#pragma omp parallel default(shared) private(i,j,k) num_threads(numprocs)
	{
		#pragma omp for nowait
		for(i=nodesize+ghostsize;i<nodesize+ghostsize+b0+b1+b2+b3+4;i++)
		{
			
		    j = nodeArray[i].NN[2];
			for(k=0;k<npt;k++)
			{				
				nodeArray[i].Phi[k] = nodeArray[j].Phi[k];
				nodeArray[i].dPdt[k] = nodeArray[j].dPdt[k];
			}

			nodeArray[i].x = nodeArray[j].x;
		    nodeArray[i].y = nodeArray[j].y;
			nodeArray[i].C = nodeArray[j].C;
			nodeArray[i].dCdt = nodeArray[j].dCdt;
			nodeArray[i].eU = nodeArray[j].eU;
		    nodeArray[i].q = nodeArray[j].q;
			nodeArray[i].u1 = nodeArray[j].u1;
			nodeArray[i].u2 = nodeArray[j].u2;
			nodeArray[i].T = nodeArray[j].T;
			nodeArray[i].sumphi = nodeArray[j].sumphi;
		}
		
		// Corners
		#pragma omp for nowait
		for(i=nodesize+ghostsize;i<nodesize+ghostsize+4;i++)
		{
		    j = nodeArray[i].NN[2];
			for(k=0;k<npt;k++)
			{
				nodeArray[i].A[k] = nodeArray[j].A[k];
				nodeArray[i].dA[k] = nodeArray[j].dA[k];
				nodeArray[i].dPhix[k] = nodeArray[j].dPhix[k];
				nodeArray[i].dPhiy[k] = nodeArray[j].dPhiy[k];
			}
			nodeArray[i].Cx = nodeArray[j].Cx;
			nodeArray[i].Cy = nodeArray[j].Cy;
		}
		//boundary 0
		#pragma omp for nowait
		for(i=nodesize+ghostsize+4;i<nodesize+ghostsize+4+b0;i++)
		{
		    j = nodeArray[i].NN[2];
			for(k=0;k<npt;k++)
			{
				nodeArray[i].A[k] = nodeArray[j].A[k];
				nodeArray[i].dA[k] = -1*nodeArray[j].dA[k];
				nodeArray[i].dPhix[k] = nodeArray[j].dPhix[k];
				nodeArray[i].dPhiy[k] = -1*nodeArray[j].dPhiy[k];
			}
			nodeArray[i].Cx = nodeArray[j].Cx;
			nodeArray[i].Cy = -1.*nodeArray[j].Cy;
		}
		//boundary 1
		#pragma omp for nowait
		for(i=nodesize+ghostsize+4+b0;i<nodesize+ghostsize+4+b0+b1;i++)
		{
		    j = nodeArray[i].NN[2];
			for(k=0;k<npt;k++)
			{
				nodeArray[i].A[k] = nodeArray[j].A[k];
				nodeArray[i].dA[k] = -1*nodeArray[j].dA[k];
				nodeArray[i].dPhix[k] = -1*nodeArray[j].dPhix[k];
				nodeArray[i].dPhiy[k] = nodeArray[j].dPhiy[k];
			}
			nodeArray[i].Cx = -1*nodeArray[j].Cx;
			nodeArray[i].Cy = nodeArray[j].Cy;
		}
		//boundary 2
		#pragma omp for nowait
		for(i=nodesize+ghostsize+4+b0+b1;i<nodesize+ghostsize+4+b0+b1+b2;i++)
		{
		    j = nodeArray[i].NN[2];
			for(k=0;k<npt;k++)
			{
				nodeArray[i].A[k] = nodeArray[j].A[k];
				nodeArray[i].dA[k] = -1*nodeArray[j].dA[k];
				nodeArray[i].dPhix[k] = nodeArray[j].dPhix[k];
				nodeArray[i].dPhiy[k] = -1*nodeArray[j].dPhiy[k];
			}
			nodeArray[i].Cx = nodeArray[j].Cx;
			nodeArray[i].Cy = -1*nodeArray[j].Cy;
		}
		//boundary 3
		#pragma omp for nowait
		for(i=nodesize+ghostsize+4+b0+b1+b2;i<nodesize+ghostsize+4+b0+b1+b2+b3;i++)
		{
		    j = nodeArray[i].NN[2];
			for(k=0;k<npt;k++)
			{
				nodeArray[i].A[k] = nodeArray[j].A[k];
				nodeArray[i].dA[k] = -1*nodeArray[j].dA[k];
				nodeArray[i].dPhix[k] = -1*nodeArray[j].dPhix[k];
				nodeArray[i].dPhiy[k] = nodeArray[j].dPhiy[k];
			}

			nodeArray[i].Cx = -1*nodeArray[j].Cx;
			nodeArray[i].Cy = nodeArray[j].Cy;
		}
	}//end openmp
}

void grid::temperature(int niter)
{ 
	double tgrad;
	int t;
	
	//begin openmp
	#pragma omp parallel default(shared) private(tgrad) num_threads(numprocs)
	{
		#pragma omp for schedule(static)
		for(t=0;t<nodesize;t++)	
		{
	        tgrad = (nodeArray[t].y - yo - dvp*niter*dt)/dlt;
	        nodeArray[t].T = Tl + tgrad*DT_o;
		}
	} //end openmp
}	
//NEW TEMPERATURE CALCULATION FUNCTION
//READING TEMPERATURE MATRIX AS A FUNCTION OF TIME AND POSITION
//The vectors:
//tvec = time values,
//yvec = yvalues, and
//Tmat = Temperature values
//must be accesible to this pont
//Also, the dimensions of the matrix noys, nots, must be known.
void grid::temperature(int niter,int flag)
{ 
	int t, j, jj;
	double time, posy;
	double y0,yf,t0,tf,dely,delt,dya,dyb,dta,dtb,tempdl,tempdr,tempul,tempur;
	double tempd,tempu,templ,tempr,tempret;
		
	//y0,tf --- yf,tf	
	//  |	      |
	//  |	      |
	//  |         |
	//y0,t0 --- yf,t0	

	time=niter*dt*tau;
		
	//begin openmp
	#pragma omp parallel default(shared) private(t,j,jj,posy,y0,yf,t0,tf,dely,delt,dya,dyb,dta,dtb,tempdl,\
			tempdr,tempul,tempur,tempd,tempu,templ,tempr,tempret) num_threads(numprocs)
	{
		#pragma omp for schedule(static)
		for(t=0;t<nodesize;t++)	
		{
			posy=W*nodeArray[t].y;
		
			//Interpolation to nodes
			tempret=0.0;
			for (j=0;j<=noys-1;j++) //Position values
			{
				for(jj=0;jj<=nots-1;jj++) //Time values
				{
					y0=yvec[j];
					yf=yvec[j+1];
					t0=tvec[jj];
					tf=tvec[jj+1];
					if ((posy>=y0) && (posy<yf) && (time>=t0) && (time<tf))
					{
						//cout << "niter, in " << niter << " in " << endl;
						dely=yf-y0;
						delt=tf-t0;
						dya=posy-y0;
						dyb=yf-posy;
						dta=time-t0;
						dtb=tf-time;
						tempdl=Tmat[j][jj];
						tempdr=Tmat[j+1][jj];
						tempul=Tmat[j][jj+1];
						tempur=Tmat[j+1][jj+1];
						tempd=(dya*tempdr+dyb*tempdl)/dely;
						tempu=(dya*tempur+dyb*tempul)/dely;
						templ=(dta*tempul+dtb*tempdl)/delt;
						tempr=(dta*tempur+dtb*tempdr)/delt;	
						tempret=(dya*tempr+dyb*templ+dta*tempu+dtb*tempd)/(dely+delt); 
					}
				}
			}

			//Imposing lower bound for temperature 
			nodeArray[t].T = max(tempret,lbTemp);
			if ((nodeArray[t].T < lbTemp) || (nodeArray[t].T > 1347.79))
			{
				cout << "Temp. out of bounds at y, T " << nodeArray[t].y << ", " << nodeArray[t].T << endl;
			}
		}
	} //end openmp	
}
	
void grid::readdata()
{
	int ii,jj;
	double dd;

	FILE * tempfile;
	tempfile=fopen("temppro.txt","rt");

	for (ii=0;ii<=noys-1;ii++)
	{
		fscanf(tempfile,"%lf",&yvec[ii]);  
	}

	for (ii=0;ii<=nots-1;ii++)
	{
		fscanf(tempfile,"%lf",&tvec[ii]);
	}
	
	for (ii=0;ii<=noys-1;ii++)
	{
		for (jj=0;jj<=nots-1;jj++)
		{
			fscanf(tempfile,"%lf",&Tmat[ii][jj]);  
		}
	}

	fclose(tempfile);
}