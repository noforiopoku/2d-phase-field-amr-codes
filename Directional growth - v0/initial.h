#ifndef INITDEF
#define INITDEF

#include <cmath>

/*******OpenMP variables********/
const int numprocs = 20;		//maximum number of threads

const double Pi = (2.*acos(0.));
extern unsigned int seed;

//variables  for initial condition of Planar interface using Sinusoidal peturbation
const int flag = 0;				//if set to 0, sinusoidal initial condition, random otherwise 	
const double yo = 20.0;			//initial offset of planar front
const double iq = 1;			//sets the number of peaks (i.e. wavelength) in the initial sinusoidal pertubration
const double kw = 2.*Pi;				
const double MagNoise = 15.0;	//magnitude of noise for planar interface

//variables for physical material parameters for the alloy and model
// Mg4%Al
const double Tm = 923.15;			//Melting temperature (K) for pure magnesium
const double cl_o = .5;				//equilibirum liquidus composition in wt%
const double m = 6.454 ;				//absolute value of the liquidus slope units of K/wt%
const double k = 0.364 ;				//partition coefficient
const double Dl = 1.8E-9;			//liquid diffusion coefficient in units of m^2/s
const double Gamma = 6.2E-7;		//gibbs-thomson coefficient in units of K*m
	 
const double Tl = Tm - m*cl_o;		//liquidus temperature
const double DT_o = m*cl_o*(1.-k);	//the freezing range as calculated per the alloy parameters
const double d_o = Gamma/DT_o;		//the capillary as described by the material parameters

//conditions for simulations
const int tempFlag = 0;				//whether to use temperautre gradient or profile from file flag ( 0 - gradient, 1- profile)
const double G = 16000.0;			//the imposed thermal gradient in units of K/m
const double vp = 0.00035;			//the pulling speed in units of m/s
const double lt = DT_o/G;			//the selected thermal length in units of m
const double W = 1.E-6;				//interface width that we wish to simulate, in units of m
const double Fu = 1.E-5;			//noise strength/amplitude of noise for adding conserved noise (i.e. random currents) to composition field
const double lbTemp = 0.0;			//Lower bound temperature for the simulation in K

/******** phase field simulation parameters ********/
////////////////////////////////////////////////////

//dimensionless variables
const double a1 = 0.8839;
const double a2 = 0.6267;
const double at = 0.35355;		//Antitrapping coefficient
const double L = a1*W/d_o;		//lambda, inverse of the nucleation barrier and convergence parameter
const double D = a2*L;			//dimensionless diffusion coefficient
const double dt = 0.004 ;		//time step, follows stability criterion dt<dx^2/4D
const double Fu_dt = Fu/dt;		//coefficient preceeding uncorrelated noise

const double tau = a2*L*W*W/Dl;		//time constant, scales atomic attachment kinetics
const double dvp = vp*tau/W;		//dimensionless pulling speed, units of W/tau
const double dlt = lt/W;			//dimensionless thermal length, units of W
const double Epsilon = 0. ;			//Ratio of solid to liquid diffusion coefficient
const double R = 8.3144621 ; 		// Gas Constant in J /( mol K )
const double Qe = 155000 ; 			// Activation energy for solid diffusion - Al diffusing in Mg (J/mol)

/*** Anisotropy Components ***/
const double sym = 4.;			//crystal symmetry (4-four fold, 6-six fold etc)	
const double Es = .03;			//anisotropy strength

//domain and size parameters
const int resolution = 8 ;				//min dx = size / 2^(resolution-1)
const double size = 100 ;				//Size of one domain in units of W
const int nx = 60 ;						//Number of domains along the x direction
const int ny = 20 ;						//Number of domains along the y direction

const int PBoundaries = 0;				// if set to 1 Periodic boundaries, non-periodic otherwise.

//runtime and output
const int printFreq = 10000 ;			//printing frequency
const int endt = 1000000 ;				//Total number of iterations

const double Beta = .0;					// parameter to include concentration gradients in adaption
const double AdaptThresh = .001;		// adaption criteria threshold
const int AdaptFreq = 10;				//adaption time cycle


/*******Multi Grain Parameters*******/
const int ng = 1 ;						//number of phase fields
const double obs = 152.173 ;			//magnitude of obstacle 
const double maxmo = Pi*15./180. ;	//maximum misorientation for RS is applicable (15 degrees)
const int oriFlag = 1;					//flag for either random (0) or uniform (non-zero) orientations for grains
										//if uniform is choosen, orientations will fall within equal ntervals within the symmetry of crystal

double * const ang = new double[ng];
double * const dumypsi = new double[ng*ng];
double ** const psi = new double*[ng];

const double RADIUS = 10.0; 		//Thickness of initial planar front

//Nucleation parameters
const int nucrun = 0;				//flag to determine if this is a nucleation run (0-no, 1-yes)
const int nucstart = 1;				//nucleation starts at this iteration
const int nucfreq =  100 ;			//nulceation frequency (nucfreq<=adapt_freq)
const double ampnuc = RADIUS+5;		//mesh range of nuclei seed

const double rho = 1.74E6;       	//Density of Mg (g/m^3)
const double mm = 24.30;         	//Atomic weight of Mg
const double v_o = mm/rho;       	//Molar volume (m^3)/mol
const double nav = 6.02214E23;   	//Avogadro's number
const double Na = nav/v_o;			//Number of atoms per volume

const double Nsl = 25.0 ;
const double fpar = pow(Nsl/(nx*ny*size*size*W*W),1.5)/Na ;	//Nucleation seeds fraction;

const double size3d = nx*nx*ny*size*size*size;	//3D system size
const int Ns = (int)(fpar*Na*size3d*W*W*W);		//Number of seeds for total 3D system
const double hetpre = 7.E-2 ;					//Heterogeneous nucleation prefactor

//There should be no reason why you would need to change anything below this comment//
const int padlevel = 0;						//Level of which the interface is padded with nodes
const double w = size;						//Width of a single domain
const double h = size;						//Height of a single domain

const double XCen = (double)size*nx/2.0;	//X Center of entire domain
const double YCen = (double)size*ny/2.0;	//Y Center of entire domain

const int Numx = 4*nx;						//Number of initial nodes needed along x
const int Numy = 4*ny;						//Number of initial nodes needed along y

//Temperature matrix dimensions
const int noys=2;								//No. position vals
const int nots=2;								//No. time vals

#endif