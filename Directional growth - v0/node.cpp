#include <cmath>
#include <stdlib.h>
#include "initial.h"
#include "node.h"
#include "time.h"

node::node(double xi, double yi,list<node *>::iterator n, list<node *>::iterator g)
{
	int i,j;
	int ii;
	
	Phi = new double[ng];

	x = xi;
	y = yi;
	C = k;
	T = 0;
	
	for (ii=0;ii<ng;ii++)
		Phi[ii] = -1.0;
    
	NN[0] = NULL;
	NN[1] = NULL;
	NN[2] = NULL;
	NN[3] = NULL;

	for(i=0;i<5;i++)
	{
	    for(j=0;j<5;j++)
	    {
			N25[i][j] = NULL;
	    }
	}
	me = n;
	gme = g;
}

node::~node()
{
	delete Phi;
}

void node::average(node *N1, node *N2)
{
	int ii;

	for(ii=0;ii<ng;ii++)
		Phi[ii] = (N1->Phi[ii] + N2->Phi[ii])/2;
	                        		
	C = (N1->C + N2->C)/2;
	T = (N1->T + N2->T)/2;
}

void node::average(node *N1, node *N2,node *N3, node *N4)
{
	int ii;

	for(ii=0;ii<ng;ii++)
		Phi[ii] = (N1->Phi[ii] + N2->Phi[ii] +N3->Phi[ii] + N4->Phi[ii])/4;
	        
	C = (N1->C + N2->C + N3->C+ N4->C)/4;
	T = (N1->T + N2->T + N3->T+ N4->T)/4;
}

void node::initialize()
{
	int ii;

	//Seed planar front for order-parameter 0
	Phi[0] = -tanh( (y-yo) - RADIUS );
	
	C = (1.+k-(1.-k)*Phi[0])/2.0;
}	

void node::nucleate(double xnuc, double ynuc, double radnuc, double Cnuc, int fnuc)
{
	double dd;
	
	dd = sqrt((x-xnuc)*(x-xnuc) + (y-ynuc)*(y-ynuc)) - radnuc;

	if( dd < 0. )
	{
		Phi[fnuc] = 1.0;
		C = Cnuc;
	}
}

void node::setNN(int i, node *n)
{
	NN[i] = n;
}
