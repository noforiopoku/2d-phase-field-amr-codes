#include <iostream>
#include <iomanip>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include "grid.h"
#include "initial.h"

using namespace std;

unsigned seed;

int main()
{
    int i,ii,jj;
	int npt=1,nucflag=0, fnuc, ncount=1;
    int adapt=0,solver=0,t;
    double xnuc,ynuc,radnuc,Cnuc;

    //seed for random number generator
	seed = time(NULL);

    //x, y and z positions for all the seeds
    double *spx = new double[Ns];
    double *spy = new double[Ns];
    double *spz = new double[Ns];

    //Innoculant occupation vector
    int *ocv = new int[Ns];

    //Assigning random position values;
    cout << "Total no. of inoculants (3D) " << Ns << endl;
    
    for(jj=1;jj<=Ns;jj++)
    {
        spx[jj] = (double)(nx*size)*(double)rand_r(&seed)/(double)RAND_MAX;
        spy[jj] = (double)(ny*size)*(double)rand_r(&seed)/(double)RAND_MAX;
        spz[jj] = (double)(nx*size)*(double)rand_r(&seed)/(double)RAND_MAX;
        ocv[jj] = 0;
    }
    
	grid *AdaptiveGrid;
    cout << "create grid" << endl;
    AdaptiveGrid = new grid(PBoundaries);
	cout << "initialize" << endl;
    AdaptiveGrid->initializeGrid();
	cout << "dx" << endl;
    AdaptiveGrid->setdx();
	cout << "Creating the Initial Solver Array" << endl;
    AdaptiveGrid->createArray(npt);
	cout << "Finished Creating Initial Solver Array" << endl;
    cout << "Initial Output" << endl;
    AdaptiveGrid->output(0,npt);
    cout << "Done Initial Output" << endl;
    
    //READING TEMPERATURE PROFILE FROM FILE
    if( tempFlag == 1)
    {
		AdaptiveGrid->readdata();
		cout << "Finished Reading Temperature profile" << endl;
	}

   	// set or randomize first orientation then cycle through orientations for nucleations
   	if( oriFlag == 0 )
   		fnuc = 1;
   	else
   		fnuc = (int)((double)(ng)*((double)rand_r(&seed)/ ( (double)RAND_MAX + 1.0)));

    for(i=1;i<=endt;i++)
    {
    	if( !tempFlag)
    		AdaptiveGrid->temperature(i);
    	else
    		AdaptiveGrid->temperature(i,tempFlag);

  		if(i%AdaptFreq == 0 )
		{
		    adapt = clock();
			AdaptiveGrid->updateGrid(npt);
			AdaptiveGrid->setdx();
			AdaptiveGrid->createArray(npt);
		    adapt = clock() - adapt;
		    t = CLOCKS_PER_SEC;

		    if(i%printFreq == 0)
		    {
			    cout << i<< "	"<< (double)(AdaptiveGrid->nodesize*AdaptFreq)/((double)solver/t) << "	" << (double)solver/t  << "	" <<  (double)adapt/t << "	" << AdaptiveGrid->nodesize << "	" << AdaptiveGrid->ghostsize << endl;
			    adapt = 0;
			    solver = 0;
			}
		}

		if( i%nucfreq == 0 && nucrun == 1 && i >= nucstart )
		{
		    nucflag = 0;
			if( !tempFlag)
    			AdaptiveGrid->temperature(i);
    		else
    			AdaptiveGrid->temperature(i,tempFlag);
    		
			AdaptiveGrid->updateGrid(npt);
			AdaptiveGrid->findNucSites(xnuc,ynuc,radnuc,Cnuc,fnuc,nucflag,npt,i,spx,spy,spz,ocv);

			if ( nucflag == 1 )
			{		
				ncount = ncount+1;
				
				if( ncount <= ng )
					npt = ncount;

				cout << "Nucleation event at " << xnuc << ", " << ynuc << " Cnuc " << Cnuc << endl;
				AdaptiveGrid->nucleate(xnuc,ynuc,radnuc,Cnuc,fnuc);
                
				AdaptiveGrid->setdx();
				AdaptiveGrid->createArray(npt);
			    
			    //Output
				AdaptiveGrid->output(i,npt);
			    cout << "Time:	" << i << endl;
			}	
		}

		t = clock();

		AdaptiveGrid->updateGhosts(1,npt);
		AdaptiveGrid->updateBC(npt);
		AdaptiveGrid->calcprePhi(npt);

		AdaptiveGrid->updateGhosts(2,npt);
		AdaptiveGrid->updateBC(npt);
		AdaptiveGrid->calcdPdt(i,npt);

		AdaptiveGrid->updateGhosts(3,npt);		
		AdaptiveGrid->updateBC(npt);
		AdaptiveGrid->calcUnoise();
		AdaptiveGrid->calcpreC(npt);

		AdaptiveGrid->updateGhosts(4,npt);
		AdaptiveGrid->updateBC(npt);
		AdaptiveGrid->calcdCdt(i,npt);

		AdaptiveGrid->step(npt);
				
 		solver += clock() - t;
	
		if(i%printFreq == 0)
		{
			//AdaptiveGrid->outputelement(i,npt);
		    AdaptiveGrid->output(i,npt);
		    cout << "Time:   " << i << endl;
		}		
	}
    
    return 0;
}	
