#ifndef ARRAYNODEDEF
#define ARRAYNODEDEF

#include <iostream>
#include <vector>
#include <list>
#include "initial.h"

using namespace std;

class arrayNode
{
    public:
        double x,y,dx;
        double Phi[ng],dPdt[ng],A[ng],dA[ng],dPhix[ng],dPhiy[ng],sumphi;
        double C,dCdt,eU,q,Cx,Cy;   //concentration flux related arrays
        double u1,u2;		        //arrays for random currents, i.e. noise

        //Create new variable T
        double T;


        int NN[4];
        int N25[5][5];
        arrayNode();
        ~arrayNode();
        void updatePhi(int npt);
        void updateC();
};


#endif
