#include <cmath>
#include <stdlib.h>
#include "initial.h"
#include "node.h"
#include "time.h"

node::node(double xi, double yi,list<node *>::iterator n, list<node *>::iterator g)
{
	int i,j;
	int ii;
	
	Phi = new double[ng];

	x = xi;
	y = yi;
	C = 1.;
	T = 0;
	
	for (ii=0;ii<ng;ii++)
		Phi[ii] = -1.0;
    
	NN[0] = NULL;
	NN[1] = NULL;
	NN[2] = NULL;
	NN[3] = NULL;

	for(i=0;i<5;i++)
	{
	    for(j=0;j<5;j++)
	    {
			N25[i][j] = NULL;
	    }
	}
	me = n;
	gme = g;
}

node::~node()
{
	delete Phi;
}

void node::average(node *N1, node *N2)
{
	int ii;

	for(ii=0;ii<ng;ii++)
		Phi[ii] = (N1->Phi[ii] + N2->Phi[ii])/2;
	                        		
	C = (N1->C + N2->C)/2;
	T = (N1->T + N2->T)/2;
}

void node::average(node *N1, node *N2,node *N3, node *N4)
{
	int ii;

	for(ii=0;ii<ng;ii++)
		Phi[ii] = (N1->Phi[ii] + N2->Phi[ii] +N3->Phi[ii] + N4->Phi[ii])/4;
	        
	C = (N1->C + N2->C + N3->C+ N4->C)/4;
	T = (N1->T + N2->T + N3->T+ N4->T)/4;
}

void node::initialize(double *Sepx,double *Sepy)
{
	int ii;
	double sumphi=0.0;
	double eu;

	if( nucrun == 0 && coolFlag == 0 )	//if this is non-nucleation run seed middle of domain
	{
		for(ii=0;ii<ng;ii++)
		{	
			Phi[ii] = -tanh((sqrt((x-XCen+Sepx[ii])*(x-XCen+Sepx[ii]) + (y-YCen+Sepy[ii])*(y-YCen+Sepy[ii]))-RADIUS)*1.0);
			sumphi = sumphi + Phi[ii];
		}
		
		eu = 1. - (1.-k)*omega;
		C = eu*(1.+k-(1.-k)*(sumphi+ng-1.) ) /2;
	}
	else if( nucrun == 0  && coolFlag == 1 )	// if non-nucleation run but cooling with grains places somewhere
	{
		for(ii=0;ii<ng;ii++)
		{	
			Phi[ii] = -tanh((sqrt((x-XCen+Sepx[ii])*(x-XCen+Sepx[ii]) + (y-YCen+Sepy[ii])*(y-YCen+Sepy[ii]))-RADIUS)*1.0);
			sumphi = sumphi + Phi[ii];
		}
		
		C = (1+k-(1-k)*(sumphi+ng-1.))/2.0; //Phi->sumPhi+ng-1
	}
	else
	{
		for(ii=0;ii<ng;ii++)
		{
			Phi[ii] = -1.;
			sumphi = sumphi + Phi[ii];
		} 
		
		C = (1+k-(1-k)*(sumphi+ng-1.))/2.0; //Phi->sumPhi+ng-1
	}
}	

void node::nucleate(double xnuc, double ynuc, double radnuc, double Cnuc, int fnuc)
{
	double dd;
	
	dd = sqrt((x-xnuc)*(x-xnuc) + (y-ynuc)*(y-ynuc)) - radnuc;

	if( dd < 0. )
	{
		Phi[fnuc] = 1.0;
		C = Cnuc;
	}
}

void node::setNN(int i, node *n)
{
	NN[i] = n;
}
