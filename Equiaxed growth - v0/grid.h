#ifndef gridDEF
#define gridDEF

#include "tree.h"
#include "node.h"
#include "arrayNode.h"

class grid
{
    node *boundary;
    list<node*> *tmpnodes;
    list<node*> *ghosts;
	list<node *>::iterator nodeNULL;
	list<node *>::iterator ghostNULL;
    arrayNode *nodeArray;

    int nTrees;
	    
    tree **AdaptiveTree;
	 
    public:
        //seed for random number generator
        //unsigned sd;

        //Temperature matrix variables
        double tvec[nots];
        double yvec[noys];
        double Tmat[noys][nots];
        
        int nodesize,ghostsize,b0,b1,b2,b3,bt;    
        list<node*> *nodes;
        grid(int Periodic,double *Sepx,double *Sepy);
        virtual ~grid();
        
        void readdata();
        void setdx();
    	void updateGrid(int npt);
        void initializeGrid(double *Sepx,double *Sepy);
        void createArray(int npt);
        void findNucSites(double& xnuc, double& ynuc, double& radnuc, double& Cnuc, int& fnuc, int& nucflag, int npt, int niter, double *spx, double *spy, double *spz, int *ocv);   
        void nucleate(double xnuc, double ynuc, double radnuc, double Cnuc, int fnuc);
        void updateGhosts(int l,int npt);
    	void calcprePhi(int npt);
        void calcdPdt(int niter,int npt);
    	void calcUnoise();
    	void calcpreC(int npt);
        void calcdCdt(int niter, int npt);
        void step(int npt);
        void outputelement(int l,int npt);
        void output(int l,int npt);
    	void updateBC(int npt);
    	void temperature(int niter);
};

#endif

